import React, { Component } from 'react';
import PropTypes from 'prop-types'; 
import * as pageActions from '../actions/page-actions'; 
import axios from 'axios'; 
import {connect} from 'react-redux';

class Pagination extends Component {
  
  nextPage() {
    // update page number
    this.props.dispatch(pageActions.updatePagination(this.props.page+1));
    axios.post('https://quiet-savannah-60607.herokuapp.com/api/artist',{artist:this.props.artist, page: this.props.page})
        .then(res => {
          this.props.dispatch(pageActions.updateTitles(res.data.message.body.track_list));
        });
  }
  
  prevPage() {
    // update page number
    this.props.dispatch(pageActions.updatePagination(this.props.page-1));
    axios.post('https://quiet-savannah-60607.herokuapp.com/api/artist',{artist:this.props.artist, page: this.props.page})
        .then(res => {
          this.props.dispatch(pageActions.updateTitles(res.data.message.body.track_list));
        });
  }
  
  render() {
    return (
          <div className="row">
            <div className="col-xs-4">
              {this.props.page > 1 && 
                <a className="btn btn-primary" onClick={() => this.prevPage()}>Previous</a>
              }
            </div>
            <div className="col-xs-4 text-center">Page {this.props.page}</div>
            <div className="col-xs-4 text-right">
                <a className="btn btn-primary" onClick={() => this.nextPage()}>Next</a>
            </div>
          </div>
    );
  }
}

Pagination.propTypes = {
  dispatch: PropTypes.func.isRequired
};

function mapStateToProps(state, props) {
  return {
    page: state.page,
    artist: state.artist
  };
}

export default connect(mapStateToProps)(Pagination);
  