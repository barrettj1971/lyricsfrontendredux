import React, { Component } from 'react';
import PropTypes from 'prop-types'; 
import * as pageActions from '../actions/page-actions'; 
import axios from 'axios'; 
import {connect} from 'react-redux';

class ResultsList extends Component {
	
  getLyrics = (lyrics_id,track_name) => {
    axios.post('https://quiet-savannah-60607.herokuapp.com/api/lyric',{lyrics_id:lyrics_id})
        .then(res => {
        	this.props.dispatch(pageActions.updateLyrics(res.data.message.body.lyrics)); 
        	this.props.dispatch(pageActions.updateTrackName(track_name));
          // if(res.data.message.body.lyrics) {
          //   this.setState({'lyrics':res.data.message.body.lyrics,'track_name':track_name});
          // } else {
          //   this.setState({'lyrics':'', 'track_name':''});
          // }
        });
  }
  
  render() {
    return (
      <li className="list-group-item" onClick={() => this.getLyrics(this.props.track.track_id, this.props.track.track_name)}>
        {this.props.track.track_name} <small>by {this.props.track.artist_name} </small> <br/>
        <small> ( From the album {this.props.track.album_name} )</small>
        
      </li>
    );
  }
}

ResultsList.propTypes = {
  dispatch: PropTypes.func.isRequired
};

function mapStateToProps(state, props) {
  return {
  	
  };
}

export default connect(mapStateToProps)(ResultsList);
  