import React, {Component} from 'react';
import Navbar from './Navbar';
import Results from './Results'; 

class PageContainer extends Component {

  render() {

    return (
      <div>
        <Navbar />
        <Results />
      </div>
    );
  }
}

export default PageContainer;
