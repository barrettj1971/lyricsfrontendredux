import React, {Component} from 'react';
import PropTypes from 'prop-types';
import * as pageActions from '../actions/page-actions'; 
import {connect} from 'react-redux';
import axios from 'axios'; 

class Navbar extends Component {
  
  constructor(props) {
    super(props); 
    this.updateArtist = this.updateArtist.bind(this);
    this.artistSearch = this.artistSearch.bind(this);
  }
  
  updateArtist() {
    const artistsearch = document.getElementById('artistsearch').value;
    this.props.dispatch(pageActions.updateArtist(artistsearch)); 
  }
  
  artistSearch(e) {
    e.preventDefault();
    // api call to get titles for this artist
    axios.post('https://quiet-savannah-60607.herokuapp.com/api/artist',{artist:this.props.artist, page: 1})
        .then(res => {
          // this.setState({'titles':res.data.message.body.track_list});
          this.props.dispatch(pageActions.updateTitles(res.data.message.body.track_list));
        });
  }
  
  render() {
    return (
      <nav className="navbar navbar-inverse">
      <div className="container">
        <div className="row">
          <div className="col-md-9">
            <a className="navbar-brand" href="">Lyrics Lookup</a>
          </div>

          <div className="col-md-3" style={{'paddingTop':'.5em'}}>
            <form onSubmit={this.artistSearch} >
              <div className="input-group">
                <input type="text" className="form-control" placeholder="Band/Artist" id="artistsearch" onKeyUp={this.updateArtist}/>
                <span className="input-group-btn">
                  <button className="btn btn-default" type="submit" >Go!</button>
                </span>
              </div>
            </form>
          </div>
        </div>
      </div>
    </nav>
    );
  }
}

Navbar.propTypes = {
  dispatch: PropTypes.func.isRequired
};

function mapStateToProps(state, props) {
  return {
    artist: state.artist
  };
}

export default connect(mapStateToProps)(Navbar);
