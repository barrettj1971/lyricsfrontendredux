import rootReducer from '../reducers/page-reducer.js';
import {createStore} from 'redux';

export default (initialState) => {
  return createStore(rootReducer, initialState);
};
