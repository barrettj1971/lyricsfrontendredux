export function updateArtist(artist){
  return {
    type: 'UPDATE_ARTIST',
    artist:artist
  };
}

export function updateTitles(titles) {
	return {
		type: 'UPDATE_TITLES',
		titles: titles
	}
}

export function updatePagination(page) {
	return {
		type: 'UPDATE_PAGE',
		page: page
	}
}

export function updateLyrics(lyrics) {
	return {
		type: 'UPDATE_LYRICS',
		lyrics: lyrics
	}
}

export function updateTrackName(track_name) {
	return {
		type: 'UPDATE_TRACK_NAME',
		track_name: track_name
	}
}
