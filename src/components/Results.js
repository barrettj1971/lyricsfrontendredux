import React, { Component } from 'react';
import ResultsList from './ResultsList';
import ResultsLyrics from './ResultsLyrics'; 
import Pagination from './Pagination'; 
import PropTypes from 'prop-types'; 
import {connect} from 'react-redux';

class Results extends Component {
  
  render() {
    let listItems;
    if(this.props.titles.length) {
      listItems = this.props.titles.map(title => {
        return (
          <ResultsList track={title.track} key={title.track.lyrics_id} onClick={this.getLyrics}/>
        );
      });
    }
  
    return (
      <div className="container">
      
        <div className="row">
          <div className="col-md-6">
            <h2>{this.props.artist}</h2>
          </div>
        </div>
        
        {this.props.titles.length>1 && 
          <Pagination />
        }
          <hr/>
        
        <div className="row">
          
          <div className="col-md-6">
            <ul className="list-group">
              {listItems}
            </ul>
          </div>
          { this.props.track_name.length > 0 && 
            <ResultsLyrics lyrics={this.props.lyrics} track_name={this.props.track_name} />
          }
          
        </div>
          
      </div>
    );
  }
}

Results.propTypes = {
  dispatch: PropTypes.func.isRequired,
  lyrics: PropTypes.object
};

function mapStateToProps(state, props) {
  return {
    lyrics: state.lyrics,
    track_name: state.track_name,
    page: state.page,
    titles: state.titles,
    artist: state.artist
  };
}

export default connect(mapStateToProps)(Results);