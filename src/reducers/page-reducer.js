let defState = {
  'lyrics': {},
  'track_name': '',
  'page': 1,
  'titles': [],
  'artist': 'Enter a band or artist to get started'
};

export default (state = defState, action) => {
  switch (action.type) {
    case 'UPDATE_ARTIST':
      return Object.assign({}, state, {
        artist: action.artist.length>0 ? action.artist : 'Enter a band or artist to get started',
        lyrics: {},
        titles: [],
        page: 1
      });
    case 'UPDATE_TITLES':
      return Object.assign({}, state, {
        titles: action.titles
      });
    case 'UPDATE_PAGE':
      return Object.assign({}, state, {
        page: action.page,
        track_name: '',
        lyrics: {}
      });
    case 'UPDATE_LYRICS': 
      return Object.assign({}, state, {
        lyrics: action.lyrics
      });
    case 'UPDATE_TRACK_NAME':
      return Object.assign({}, state, {
        track_name: action.track_name
      });
    default:
      return state;
  }
};
